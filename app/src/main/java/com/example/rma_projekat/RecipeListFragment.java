package com.example.rma_projekat;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.List;

public class RecipeListFragment extends Fragment {

    private LinearLayout recipeContainer;
    private DatabaseHelper db;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recipe_list, container, false);

        recipeContainer = view.findViewById(R.id.recipeContainer);
        Button buttonAddRecipe = view.findViewById(R.id.buttonAddRecipe);
        Button buttonViewTable = view.findViewById(R.id.buttonViewTable);
        SearchView searchView = view.findViewById(R.id.searchView);

        db = new DatabaseHelper(getContext());

        buttonAddRecipe.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), AddEditRecipeActivity.class);
            startActivity(intent);
        });

        buttonViewTable.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), RecipeTableActivity.class);
            startActivity(intent);
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                loadRecipes(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                loadRecipes(newText);
                return false;
            }
        });

        loadRecipes("");

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadRecipes("");
    }

    private void loadRecipes(String filter) {
        recipeContainer.removeAllViews();
        List<Recipe> recipes = db.getAllRecipes(filter);

        for (int i = 0; i < recipes.size(); i++) {
            Recipe recipe = recipes.get(i);
            View recipeView = getLayoutInflater().inflate(R.layout.item_recipe, null);

            if (i % 2 == 0) {
                recipeView.setBackgroundColor(getResources().getColor(R.color.lightPurple));
            } else {
                recipeView.setBackgroundColor(getResources().getColor(R.color.lightBlue));
            }

            recipeView.setOnClickListener(v -> {
                FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                Bundle bundle = new Bundle();
                bundle.putInt("recipeId", recipe.getId());

                RecipeDetailFragment recipeDetailFragment = new RecipeDetailFragment();
                recipeDetailFragment.setArguments(bundle);

                fragmentTransaction.replace(R.id.recipeContainer, recipeDetailFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            });


            recipeView.findViewById(R.id.buttonEditRecipe).setBackgroundColor(getResources().getColor(R.color.lightPink));
            recipeView.findViewById(R.id.buttonDeleteRecipe).setBackgroundColor(getResources().getColor(R.color.lightPink));


            recipeView.findViewById(R.id.buttonEditRecipe).setOnClickListener(v -> {
                Intent intent = new Intent(getActivity(), AddEditRecipeActivity.class);
                intent.putExtra("recipeId", recipe.getId());
                startActivity(intent);
            });

            recipeView.findViewById(R.id.buttonDeleteRecipe).setOnClickListener(v -> {
                db.deleteRecipe(recipe.getId());
                loadRecipes("");
            });

            // Populate the view with recipe data
            ((TextView) recipeView.findViewById(R.id.recipeName)).setText(recipe.getName());
            ((TextView) recipeView.findViewById(R.id.recipeIngredients)).setText(recipe.getIngredients());

            recipeContainer.addView(recipeView);
        }
    }
}
