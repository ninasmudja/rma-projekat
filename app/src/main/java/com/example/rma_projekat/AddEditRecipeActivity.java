package com.example.rma_projekat;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

public class AddEditRecipeActivity extends AppCompatActivity {

    private DatabaseHelper db;
    private EditText editTextName, editTextIngredients, editTextInstructions;
    private Spinner spinnerImage;
    private int recipeId = -1;

    private int[] imageResIds = {R.drawable.cizkejk, R.drawable.brownie ,R.drawable.banana_torta ,R.drawable.puding ,R.drawable.pita};
    private String[] imageNames = {"Cizkejk", "Brownie", "Banana torta", "Puding", "Pita"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_recipe);

        db = new DatabaseHelper(this);

        editTextName = findViewById(R.id.editTextName);
        editTextIngredients = findViewById(R.id.editTextIngredients);
        editTextInstructions = findViewById(R.id.editTextInstructions);
        spinnerImage = findViewById(R.id.spinnerImage);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, imageNames);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerImage.setAdapter(adapter);

        recipeId = getIntent().getIntExtra("recipeId", -1);

        if (recipeId != -1) {
            Recipe recipe = db.getRecipe(recipeId);
            if (recipe != null) {
                editTextName.setText(recipe.getName());
                editTextIngredients.setText(recipe.getIngredients());
                editTextInstructions.setText(recipe.getInstructions());
                int imageResId = recipe.getImageResId();
                int position = getImagePosition(imageResId);
                spinnerImage.setSelection(position);
            }
        }

        Button buttonSaveRecipe = findViewById(R.id.buttonSaveRecipe);
        buttonSaveRecipe.setOnClickListener(v -> {
            String name = editTextName.getText().toString();
            String ingredients = editTextIngredients.getText().toString();
            String instructions = editTextInstructions.getText().toString();

            if (TextUtils.isEmpty(name) || TextUtils.isEmpty(ingredients) || TextUtils.isEmpty(instructions)) {
                Toast.makeText(AddEditRecipeActivity.this, "Popunite sva polja", Toast.LENGTH_SHORT).show();
                return;
            }

            int imageResId = imageResIds[spinnerImage.getSelectedItemPosition()];

            if (recipeId == -1) {
                db.addRecipe(new Recipe(0, name, ingredients, instructions, imageResId));
                Toast.makeText(AddEditRecipeActivity.this, "Recept dodat", Toast.LENGTH_SHORT).show();
            } else {
                Recipe recipe = new Recipe(recipeId, name, ingredients, instructions, imageResId);
                db.updateRecipe(recipe);
                Toast.makeText(AddEditRecipeActivity.this, "Recept izmenjen", Toast.LENGTH_SHORT).show();
            }
            finish();
        });
    }

    private int getImagePosition(int imageResId) {
        for (int i = 0; i < imageResIds.length; i++) {
            if (imageResIds[i] == imageResId) {
                return i;
            }
        }
        return 0;
    }
}
