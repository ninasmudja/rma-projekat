package com.example.rma_projekat;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

public class RecipeDetailFragment extends Fragment {

    private TextView textViewName, textViewIngredients, textViewInstructions;
    private ImageView imageView;
    private DatabaseHelper db;
    private int recipeId;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recipe_detail, container, false);

        textViewName = view.findViewById(R.id.textViewName);
        textViewIngredients = view.findViewById(R.id.textViewIngredients);
        textViewInstructions = view.findViewById(R.id.textViewInstructions);
        imageView = view.findViewById(R.id.imageView);

        db = new DatabaseHelper(requireContext());

        if (getArguments() != null) {
            recipeId = getArguments().getInt("recipeId", -1);
        }

        if (recipeId != -1) {
            Recipe recipe = db.getRecipe(recipeId);
            if (recipe != null) {
                textViewName.setText(recipe.getName());
                textViewIngredients.setText(recipe.getIngredients());
                textViewInstructions.setText(recipe.getInstructions());
                imageView.setImageResource(recipe.getImageResId());
            } else {
                Toast.makeText(requireContext(), "Recept nije pronadjen", Toast.LENGTH_SHORT).show();
                requireActivity().getSupportFragmentManager().popBackStack();
            }
        } else {
            Toast.makeText(requireContext(), "Recept nije pronadjen", Toast.LENGTH_SHORT).show();
            requireActivity().getSupportFragmentManager().popBackStack();
        }

        return view;
    }

    private boolean isDetailFragmentVisible(int recipeId) {
        RecipeDetailFragment fragment = (RecipeDetailFragment) requireActivity()
                .getSupportFragmentManager()
                .findFragmentById(R.id.recipeContainer);

        if (fragment != null && fragment.isVisible()) {
            int displayedRecipeId = fragment.getArguments().getInt("recipeId", -1);
            return displayedRecipeId == recipeId;
        }
        return false;
    }

    private void refreshDetailFragment(int recipeId) {
        RecipeDetailFragment fragment = new RecipeDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("recipeId", recipeId);
        fragment.setArguments(bundle);

        FragmentTransaction fragmentTransaction = requireActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.recipeContainer, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

}
