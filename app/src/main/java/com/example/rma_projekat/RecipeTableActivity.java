package com.example.rma_projekat;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

public class RecipeTableActivity extends AppCompatActivity {

    private DatabaseHelper db;
    private TableLayout tableLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_table);

        db = new DatabaseHelper(this);
        tableLayout = findViewById(R.id.tableLayout);

        loadRecipes();
    }

    private void loadRecipes() {
        List<Recipe> recipes = db.getAllRecipes("");

        for (Recipe recipe : recipes) {
            TableRow row = new TableRow(this);

            ImageView imageView = new ImageView(this);
            imageView.setImageResource(recipe.getImageResId());
            TableRow.LayoutParams lp = new TableRow.LayoutParams(200, 200);
            imageView.setLayoutParams(lp);

            TextView textView = new TextView(this);
            textView.setText(recipe.getName());
            textView.setPadding(16, 0, 0, 0);

            row.addView(imageView);
            row.addView(textView);

            tableLayout.addView(row);
        }
    }
}

